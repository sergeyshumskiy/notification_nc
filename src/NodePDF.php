<?php

/**
 * @file
 * Contains \Drupal\notification_nc\EventSubscriber\NodeCreateSubscriber.
 */

namespace Drupal\notification_nc;

use Drupal\Core\Entity\EntityTypeManager;
use \Dompdf\Dompdf;
use Masterminds\HTML5;
use Drupal\Component\Utility\Unicode;
use \Drupal\Core\Render\Markup;

/**
 * Event subscriptions for events dispatched by SimpleFbConnect.
 */
class NodePDF {

  protected $entityTypeManager;
  protected $node;

  public function __construct(EntityTypeManager $entity) {
    $this->entityTypeManager = $entity->getStorage('node');
  }

  public function setNode($node_id){
    $this->node = $this->entityTypeManager->load($node_id);
  }

  public function getNode(){
    return $this->node;
  }

  public function getFileAttachment() {
    $node = $this->getNode();

    $path_public = \Drupal::service('file_system')
      ->realpath(file_default_scheme() . "://");

    // get content from file node-pdf.css
    $css_file_content = file_get_contents(\Drupal::service('file_system')
        ->realpath() . '/'
      . drupal_get_path('module', 'notification_nc')
      . '/css/node-pdf.css');

    $view = node_view($node, 'PDF');
    $output_view = \Drupal::service('renderer')->renderRoot($view);
    $markup = Markup::create($this->getHtmlString($output_view));

    $node_pdf = [
      '#theme' => 'node_pdf',
      '#title' => $node->label(),
      '#content' => $markup,
      '#node_pdf_css' => $css_file_content,
    ];

    $html = \Drupal::service('renderer')->renderRoot($node_pdf);

    $dompdf = new Dompdf();
    $dompdf->loadHtml($html);
    $dompdf->render();
    $output = $dompdf->output();

    $file_name = 'node-' . $node->id() . '.pdf';

    $this->saveFile($output, $file_name);

    $attachment = new \stdClass();
    $attachment->uri = $path_public . '/' . $file_name;
    $attachment->filename = $file_name;
    $attachment->filemime = 'application/pdf';

    return $attachment;
  }

  public function saveFile($content, $name){
    $file = file_save_data($content, 'public://' . $name, FILE_EXISTS_REPLACE);
    if (!$file) {
      \Drupal::logger('Save file PDF')
        ->critical('File has not been saved! @file_name', ['@file_name' => $name]);
    }
  }

  public function getHtmlString($html_string) {
    $html5 = new HTML5();
    $document = $html5->loadHTML($html_string);

    $transform = function ($tag, $attribute) use ($document) {
      $base_url = \Drupal::request()->getSchemeAndHttpHost();
      $system_path = \Drupal::service('file_system')->realpath();

      // Define a function that will convert root relative uris into absolute
      // urls.
      foreach ($document->getElementsByTagName($tag) as $node) {
        $attribute_value = $node->getAttribute($attribute);

        if ($tag === 'img') {
          $attribute_value = array_shift(explode('?', $attribute_value));

          if (Unicode::substr($attribute_value, 0, 2) === '//') {
            $node->setAttribute($attribute, $system_path . Unicode::substr($attribute_value, 1));
          }
          elseif (Unicode::substr($attribute_value, 0, 1) === '/') {
            $node->setAttribute($attribute, $system_path . $attribute_value);
          }
        }
        else {
          if (Unicode::substr($attribute_value, 0, 2) === '//') {
            $node->setAttribute($attribute, $base_url . Unicode::substr($attribute_value, 1));
          }
          elseif (Unicode::substr($attribute_value, 0, 1) === '/') {
            $node->setAttribute($attribute, $base_url . $attribute_value);
          }
        }
      }
    };

    // Transform stylesheets, links and images.
    $transform('link', 'href');
    $transform('a', 'href');
    $transform('img', 'src');

    $html_string = $html5->saveHTML($document);
    $html_string = array_pop(explode('<html>', $html_string));
    $html_string = array_shift(explode('</html>', $html_string));

    return $html_string;
  }

}
