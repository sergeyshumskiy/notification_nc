<?php

/**
 * @file
 * Contains notification_nc.module.
 */

/**
 * Implements hook_theme().
 */
function notification_nc_theme($existing, $type, $theme, $path) {
  return [
    'node_pdf' => [
      'path' => $path . '/templates',
      'template' => 'node-pdf',
      'variables' => [
        'title' => '',
        'content' => NULL,
        'node_pdf_css' => NULL,
      ],
      'base hook' => 'node_pdf',
    ],
  ];
}

/**
 * Implements hook_entity_insert().
 */
function notification_nc_node_insert(Drupal\Core\Entity\EntityInterface $node) {
  $node_status = $node->getFields()['status']->value;
  if ($node_status == 0) {
    return;
  }

  $node_type = $node->bundle();
  $msg_title = $node->label();
  $msg_body = t('Node of the content type --- @node_type --- is created.', ['@node_type' => $node_type]);

  $users = getActiveUsers();

  $queue = \Drupal::queue('notification_sending_node_create');
  $queue->createQueue();

  $service = \Drupal::service('notification_nc.node_to_pdf');
  $service->setNode($node->id());
  $file = $service->getFileAttachment();

  foreach ($users as $user) {
    $queue->createItem([
      'name' => $user->name,
      'mail' => $user->mail,
      'title' => $msg_title,
      'body' => $msg_body,
      'file' => $file,
    ]);
  }
}

/**
 * Returns array with active users, and not null mail.
 *
 * @return mixed
 */
function getActiveUsers() {
  $query = \Drupal::database()
    ->select('users_field_data', 'u')
    ->fields('u', ['name', 'mail'])
    ->isNotNull('mail')
    ->condition('u.status', 1);
  $result = $query->execute()->fetchAll();

  return $result;
}

/**
 * Implements hook_mail().
 */
function notification_nc_mail($key, &$message, $params) {
  if ($key != 'node_insert') {
    return;
  }

  $options = [
    'langcode' => $message['langcode'],
  ];
  $message['from'] = \Drupal::config('system.site')->get('mail');
  $message['subject'] = t('Hello @name! Node created: @title', [
    '@title' => $params['node_title'],
    '@name' => $params['name'],
  ], $options);
  $message['body'][] = $params['message'];
  $message['params']['files'] = [array_pop($params['attachment'])];
}

/**
 * Implements hook_cron().
 */
function notification_nc_cron() {
  $queue = \Drupal::queue('notification_sending_node_create');

  $batch_operations = [];
  while ($item = $queue->claimItem()) {
    $batch_operations[] = [
      'sendMail',
      [$item],
    ];
    $queue->deleteItem($item);
  }

  $batch = [
    'operations' => $batch_operations,
  ];
  batch_set($batch);
}

/**
 * Sends mail
 *
 * @param \Drupal\Core\Queue\DatabaseQueue $item
 */
function sendMail($item) {
  $mailManager = \Drupal::service('plugin.manager.mail');
  $module = 'notification_nc';
  $key = 'node_insert';
  $mail_to = $item->data['mail'];
  $params['name'] = $item->data['name'];
  $params['node_title'] = $item->data['title'];
  $params['message'] = $item->data['body'];
  $params['attachment'][] = $item->data['file'];
  $langcode = \Drupal::currentUser()->getPreferredLangcode();

  $result = $mailManager->mail($module, $key, $mail_to, $langcode, $params);

  if ($result['result'] == 1) {
    drupal_set_message(t('Your message has been sent.'));
    \Drupal::logger('CRON')->info('Your message has been sent.');

    return;
  }

  drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
  \Drupal::logger('CRON')
    ->warning('There was a problem sending your message and it was not sent.');
}
