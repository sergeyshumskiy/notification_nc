Notification Node Create module is designed to send a notification to all active
users, messages about the created node and data, this node. Also, using this
module data about this node is stored in *.pdf format.

In order for this module to work it is necessary to do the following

1. composer require phenx/php-svg-lib:0.1
2. composer require dompdf/dompdf:0.7.0-beta3
3. composer require drupal/swiftmailer
